"""
Demonstration of how to use the inline editor in a Flask App
"""

import flask
import pathlib
import inline_editor

app = flask.Flask(__name__,
                  template_folder='templates',
                  static_folder='static',
                  static_url_path='/assets')

UPLOAD_DIR : pathlib.Path = pathlib.Path(app.instance_path) / 'uploads'
STORAGE = str(pathlib.Path(app.instance_path) / 'inline_edits.db')

inline_editor.init_app(app,storage=STORAGE)

#@inline_editor.editable_if
#def user_logged_in():
#    return True

@app.route('/')
def index():
    return flask.render_template('index.html')



app.config['CKEDITOR_FILE_UPLOADER'] = 'upload'  # this value can be endpoint or url

@app.route('/files/<path:filename>')
def uploaded_files(filename):
    return flask.send_from_directory(UPLOAD_DIR, filename)

@app.route('/upload', methods=['POST'])
def upload():

    f = flask.request.files.get('upload')

    # Add more validations here
    extension = f.filename.split('.')[-1].lower()
    allowed_extensions = ['jpg', 'gif', 'png', 'jpeg']

    if extension not in allowed_extensions:
        bad_extention_msg = f'File extension must be one of {", ".join(allowed_extensions)}!'
        return inline_editor.flask_ckeditor.upload_fail(message=bad_extention_msg)

    if not UPLOAD_DIR.exists(): UPLOAD_DIR.mkdir(parents=True)
    
    f.save(UPLOAD_DIR / f.filename)

    url = flask.url_for('uploaded_files', filename=f.filename)

    return inline_editor.flask_ckeditor.upload_success(url=url)  # return upload_success call


if __name__ == '__main__':
    app.run(debug=True)
