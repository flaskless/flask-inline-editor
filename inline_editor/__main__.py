from flask.templating import render_template, render_template_string



def create_app(html_file='index.html',database=None,static_prefix=None):
    import flask
    import pathlib, os
    import inline_editor

    FILES_PREFIX = os.environ.get('INLINE_FILES_PREFIX',os.getcwd())
    if not static_prefix:
        static_prefix = os.environ.get('STATIC_PREFIX')
    html_file_path = pathlib.Path(html_file)
    if not html_file_path.is_absolute():
        html_file_path = pathlib.Path(FILES_PREFIX) / html_file_path
    print(f"Service file at path {html_file_path.parent}")
    UPLOAD_DIR : pathlib.Path = html_file_path.parent / 'uploads'
    STORAGE = str(pathlib.Path.cwd() / f'{html_file}.db')
    if database is not None:
        STORAGE = database

    static_folder = None
    static_url_path = '/static'
    if static_prefix:
        static_folder=str(html_file_path.parent / static_prefix)
        static_url_path= f'/{static_prefix}'

    app = flask.Flask(__name__
        ,static_folder=static_folder
        ,static_url_path=static_url_path)

    app.jinja_env.block_start_string = '<!--|%'
    app.jinja_env.block_end_string = '%|-->'
    app.jinja_env.variable_start_string = '<!--|{' 
    app.jinja_env.variable_end_string = '}|-->'
    
    def is_editable():
        return flask.request.args.get('editable') is not None
    
    inline_editor.init_app(app,storage=STORAGE,is_editable=is_editable)

    @app.route('/',defaults={'path':'index.html'},methods=['GET'])
    @app.route('/<path:path>',methods=['GET'])
    def index(path):
        if not path.endswith('.html'):
            print(f"Getting static file {path}")
            return flask.send_from_directory(html_file_path.parent,path)
        if not os.path.isabs(path):
            path = os.path.join(html_file_path.parent,path)
        with open(path) as f:
            html = f.read()
        return flask.render_template_string(html)

    app.config['CKEDITOR_FILE_UPLOADER'] = 'upload'  # this value can be endpoint or url

    @app.before_first_request
    def init_db():
        db = flask.current_app.extensions['sqlalchemy'].db
        if db.engine.driver.startswith('pysqlite'):
            storage_path = pathlib.Path(db.engine.url.database)
            if not storage_path.exists():
                storage_path.touch()
                db.create_all()

    @app.route('/favicon.ico')
    def favicon():
        return 'No Favicon',200

    @app.route('/files/<path:filename>')
    def uploaded_files(filename):
        return flask.send_from_directory(UPLOAD_DIR, filename)

    @app.route('/upload', methods=['POST'])
    def upload():

        f = flask.request.files.get('upload')

        # Add more validations here
        extension = f.filename.split('.')[-1].lower()
        allowed_extensions = ['jpg', 'gif', 'png', 'jpeg']

        if extension not in allowed_extensions:
            bad_extention_msg = f'File extension must be one of {", ".join(allowed_extensions)}!'
            return inline_editor.flask_ckeditor.upload_fail(message=bad_extention_msg)

        if not UPLOAD_DIR.exists(): UPLOAD_DIR.mkdir(parents=True)
        
        f.save(UPLOAD_DIR / f.filename)

        url = flask.url_for('uploaded_files', filename=f.filename)

        return inline_editor.flask_ckeditor.upload_success(url=url)  # return upload_success call
    
    @app.route('/_freeze')
    def freeze():
        import flask_frozen
        freezer = flask_frozen.Freezer(with_no_argument_rules=False, log_url_for=False)
    
        @freezer.register_generator
        def your_generator_here():
            yield "/index.html"
        flask.current_app.config['FREEZER_DESTINATION'] = pathlib.Path.cwd() / 'build'
        freezer.init_app(flask.current_app)

        freezer.freeze()


        return 'Done'

    return app

def main():
    import sys
    filepath = 'index.html'
    database = 'inline_editor.db'
    if len(sys.argv) > 1:
        filepath = sys.argv[1]
    if len(sys.argv) > 2:
        database = sys.argv[2]

    app = create_app(filepath,database=database)
    app.run(debug=True,port=5055,use_reloader=False)

if __name__ == '__main__':
    main()