"""
A Flask extention for creating editable content areas within the app.

# Use:
* See the /about route and about.html template

# Features:
* Content is rendered via jinja before displaying
* Rendering Errors are shown above the rendered content
* New template context processor to easily create editable content

# Inspiration:
* https://github.com/hack4impact/flask-base - inline ckeditor that requires authentication
* https://github.com/greyli/flask-ckeditor - ckeditor flask extension that simplifies ckeditor configuration
* https://github.com/nathancahill/Flask-Edits - use custom jinja2 tag to create editable content areas automatically

# Posible Usages:
* Create new content pages
* Edit the menu/navigations
* Modify the Logo/Brand
* Edit the footer text

# TODO:
* Add bubbles when user clicks on link to show the apps endpoints
* Add custom jinja2 block to create editable sections like Flask-Edits
  * [Idea] Edited content should be injected by projeny directly into the block.  
    This way the template file is actually storing the content, so it cant get lost.
* Improve error handling - show nicer messages next to unrendered jinja content when there is error
* Integration with Flask-Admin
* Admin should be able to define editable sections per-role e.i RBAC for editing
* CKEditor sourcedialog should be nicely formated and highlighted HTML
* Rendered jinja2 content should be displayed immediately upon save
* Source dialog should always show jinja2 raw, not rendered.
* Saving should always save jinja2 raw, not rendered
* Edit and Save buttons should be customizable
* Automatically call ckeditor_inline.config() and ckeditor_inline.load()
* Abstraction for where content is stored. e.g in files or in database or in s3 etc.

"""

# TODO: Handle errors when inline saveing

from flask import Blueprint, request, Markup, current_app, url_for, render_template_string
import flask
import flask_ckeditor

html_editor = Blueprint(
    'html_editor',
    __name__, 
    template_folder='templates',
    static_folder='static',
    static_url_path='/html_editor/static')

class ContentMixin(object):
    '''
    This provides default implementations for the methods that Flask-Inline-Editor
    expects editable objects to have.
    '''

    # Python 3 implicitly set __hash__ to None if we override __eq__
    # We set it back to its default implementation
    __hash__ = object.__hash__

    @property
    def is_editable(self):
        return True

    @property
    def value(self):
        return ''

    def get_id(self):
        try:
            return str(self.id)
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')

    def __eq__(self, other):
        '''
        Checks the equality of two `ContentMixin` objects using `get_id`.
        '''
        if isinstance(other, ContentMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        '''
        Checks the inequality of two `ContentMixin` objects using `get_id`.
        '''
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal

@html_editor.route('/_update_editor_contents', methods=['POST'])
def update_editor_contents():
    """Update the contents of an editor."""

    edit_data = request.form.get('edit_data')
    editor_name = request.form.get('editor_name')

    current_editor.save_content(editor_name,edit_data)
    
    return 'OK', 200


def add_jinja_blocks(app):
    import jinjatag

    jinja_tag = jinjatag.JinjaTag()
    
    # jinja_options is an ImmutableDict, so we have to do this song and dance
    app.jinja_options = app.jinja_options.copy()
    if 'extensions' not in app.jinja_options:
        app.jinja_options['extensions'] = []
    app.jinja_options['extensions'].append(jinja_tag)
    jinja_tag.env = app.jinja_env
    jinja_tag.init() 

    @jinjatag.simple_block()
    def editablehtml(content, name):
        edited_content = current_editor.load_content(name)
        if edited_content.value == '':
            edited_content.value = content

        if current_editor._editable_callback is not None:
            can_edit = current_editor._editable_callback()
            if not can_edit:
                return render_template_string(edited_content.value)
        
        error = ''
        import jinja2
        try:
            rendered_value = render_template_string(edited_content.value)
        except jinja2.UndefinedError as e:
            rendered_value = content.value
            error = e

        display_buttons = flask.current_app.config.get('INLINE_EDITOR_SHOW_BUTTONS',True)

        return current_editor.create(
             name=edited_content.get_id(), 
             value=rendered_value, 
             error=error,
             display_buttons=display_buttons) + current_editor.config(
             name=edited_content.get_id(),
             custom_config="allowedContent: true", 
             postUrl=url_for('html_editor.update_editor_contents'), 
             )

def editable_content(name):
    """
    Used as a jinja2 context processor to create
    editable content in a template without any
    other code
    """
    content = current_editor.load_content(name)

    return content.value

class InlineEditor():
    def __init__(self,app=None,is_editable=None):

        self._load_callback = None
        self._save_callback = None
        self._editable_callback = is_editable
        if app:
            self.init_app(app)
                
    def is_editable(self, callback):
        '''
        This sets the callback for checking if the editor should be displayed. The
        function you set should take a content ID (a ``unicode``) and return a
        content object, or ``None`` if the content does not exist.

        :param callback: The callback for retrieving a content object.
        :type callback: callable
        '''
        self._editable_callback = callback
        return callback      

    def content_loader(self, callback):
        '''
        This sets the callback for loading content. The
        function you set should take a content ID (a ``unicode``) and return a
        content object, or ``None`` if the content does not exist.

        :param callback: The callback for retrieving a content object.
        :type callback: callable
        '''
        self._load_callback = callback
        return callback

    def load_content(self,id):
        '''Loads content'''

        if self._load_callback is None:
            raise Exception(
                "Missing content_loader. Refer to "
                "http://flask-inline-editor.readthedocs.io/#how-it-works "
                "for more info.")

        content = self._load_callback(id)

        after_loaded.send(current_app._get_current_object(),id=id,content=content)
        return content

    def save_content(self,id,content):
        '''Save content from session'''

        if self._save_callback is None:
            raise Exception(
                "Missing content_saver. Refer to "
                "http://flask-inline-editor.readthedocs.io/#how-it-works "
                "for more info.")

        before_saved.send(current_app._get_current_object(),id=id,content=content)

        content = self._save_callback(id,content)

        return content

    def content_saver(self, callback):
        '''
        This sets the callback for loading content. The
        function you set should take a content ID (a ``unicode``) and return a
        content object, or ``None`` if the content does not exist.

        :param callback: The callback for retrieving a content object.
        :type callback: callable
        '''
        self._save_callback = callback
        return callback

    def create(self,*args,**kwargs):
        return flask.current_app.extensions['ckeditor_inline'].create(*args,**kwargs)
    
    def config(self,*args,**kwargs):
        return flask.current_app.extensions['ckeditor_inline'].config(*args,**kwargs)

    def init_app(self, app: flask.Flask,is_editable=None,show_buttons=None):

        if 'csrf' not in app.extensions:
            import flask_wtf
            flask_wtf.CSRFProtect(app)

        self._editable_callback = is_editable or self._editable_callback

        add_jinja_blocks(app)

        app.config['CKEDITOR_ENABLE_CSRF'] = True  # if you want to enable CSRF protect, uncomment this line
        if show_buttons is not None:
            app.config['INLINE_EDITOR_SHOW_BUTTONS'] = show_buttons
        ckeditor = flask_ckeditor.CKEditor(app)
        ckeditor_inline = _CKEditorInline()
        if self._editable_callback:
            ckeditor_inline.is_editable = self._editable_callback
        app.extensions['ckeditor_inline'] = ckeditor_inline
        app.extensions['inline-editor'] = self

        def inline_context_processor():
            return {
                'inline_ckeditor': app.extensions['ckeditor_inline'],
                'ckeditor_js': app.extensions['ckeditor_inline'].load(),
                'editable': editable_content
                }
        
        app.context_processor(inline_context_processor)

        app.register_blueprint(html_editor)


class _CKEditorInline(flask_ckeditor._CKEditor):

    @staticmethod
    def create(name='ckeditor', value='', error='', display_buttons=True):
        """Create a ckeditor textarea directly.

        :param name: The name attribute of CKEditor textarea, set it when you need to create
            more than one textarea in one page. Default to ``ckeditor``.
        :param value: The preset value for textarea.

        .. versionadded:: 0.3
        """
        if error:
            error = Markup(f"""
                <h1 class="error-{name}">
                {error}
                </h1>
            """)
        
        editable = Markup(f"""
        <div id="{name}" contenteditable="false">
        {value}
        </div>
        """)

        buttons = ""
        if display_buttons:
            buttons = Markup("""
        <style>
                .inline-edit-button {{
                    background-color: #4CAF50; /* Green */
                    transition-duration: 0.4s;
                }}

                .inline-edit-button:hover {{
                    color: white;
                }}
        </style>

        <a class="inline-edit-button button btn btn-app btn-flat start-edit-{name}">
            <i class="fas fa-edit"></i> Edit
        </a>
        <a class="inline-edit-button button btn btn-app btn-flat end-edit-{name}">
            <i class="fas fa-save"></i> Save
        </a>
        """)

        return error + editable + buttons

    @staticmethod
    def load(custom_url=None, pkg_type='full-all', serve_local=None, version='4.16.0'):
        """
        Need to load the latest ckeditor get be able to inline-edit the source code for ckeditor.
        Also add jquery requirement.
        """

        jquery = Markup(f"""<script src="{url_for('html_editor.static', filename='scripts/vendor/jquery.min.js')}"></script>""")
        #ckeditor = flask_ckeditor._CKEditor.load(pkg_type=pkg_type,serve_local=serve_local,version=version)
        ckeditor =  Markup(f'<script src="https://cdn.ckeditor.com/{version}/{pkg_type}/ckeditor.js"></script>')
        return jquery + Markup('\n') + ckeditor

    def is_editable(self):
        return True

    @staticmethod  # noqa
    def config(name='ckeditor', custom_config='', postUrl='/', **kwargs):
        """Config CKEditor.

        :param name: The target input field's name. If you use Flask-WTF/WTForms, it need to set
            to field's name. Default to ``'ckeditor'``.
        :param custom_config: The addition config, for example ``uiColor: '#9AB8F3'``.
            The proper syntax for each option is ``configuration name : configuration value``.
            You can use comma to separate multiple key-value pairs. See the list of available
            configuration settings on
            `CKEditor documentation <https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config>`_.
        :param kwargs: Mirror arguments to overwritten configuration variables, see docs for more details.

        .. versionadded:: 0.3
        """
        from flask_ckeditor.utils import get_url
        import warnings
        extra_plugins = kwargs.get('extra_plugins', current_app.config['CKEDITOR_EXTRA_PLUGINS'])

        file_uploader = kwargs.get('file_uploader', current_app.config['CKEDITOR_FILE_UPLOADER'])
        file_browser = kwargs.get('file_browser', current_app.config['CKEDITOR_FILE_BROWSER'])
        ckfunction = 'inline'

        if file_uploader != '':
            file_uploader = get_url(file_uploader)
        if file_browser != '':
            file_browser = get_url(file_browser)

        if file_uploader or file_browser:
            if 'filebrowser' not in extra_plugins:
                extra_plugins.append('filebrowser')

        language = kwargs.get('language', current_app.config['CKEDITOR_LANGUAGE'])
        height = kwargs.get('height', current_app.config['CKEDITOR_HEIGHT'])
        width = kwargs.get('width', current_app.config['CKEDITOR_WIDTH'])

        code_theme = kwargs.get('code_theme', current_app.config['CKEDITOR_CODE_THEME'])

        wrong_key_arg = kwargs.get('codesnippet', None)
        if wrong_key_arg:
            warnings.warn('Argument codesnippet was renamed to enable_codesnippet and will be removed in future.')

        enable_codesnippet = kwargs.get('enable_codesnippet', wrong_key_arg) or \
            current_app.config['CKEDITOR_ENABLE_CODESNIPPET']

        if enable_codesnippet and 'codesnippet' not in extra_plugins:
            extra_plugins.append('codesnippet')

        enable_fixedui = kwargs.get('fixedui',True)
        if enable_fixedui:
            if 'divarea' not in extra_plugins:
                extra_plugins.append('divarea')
            ckfunction = 'replace'

        # Enable sourcedialog is you want to edit HTML directly.
        # This doesn't work with the current version of flask-ckeditor
        #if 'sourcedialog' not in extra_plugins:
        #    extra_plugins.append('sourcedialog')

        if 'uploadimage' not in extra_plugins:
            extra_plugins.append('uploadimage')

        enable_csrf = kwargs.get('enable_csrf', current_app.config['CKEDITOR_ENABLE_CSRF'])

        if enable_csrf:
            if 'csrf' not in current_app.extensions:
                raise RuntimeError("CSRFProtect is not initialized. It's required to enable CSRF protect, \
                    see docs for more details.")
            csrf_header = '''
                fileTools_requestHeaders: {
                    'X-CSRFToken': '{{ csrf_token() }}',
                },'''.replace('{{ csrf_token() }}',current_app.jinja_env.globals['csrf_token']())
        else:
            csrf_header = ''

        return Markup('''
        
<script type="text/javascript">

$(document).ready(function() {
    var editorIDName = "%(name)s";
    var startEdit = ".start-edit"+"-"+editorIDName;
    var endEdit = ".end-edit"+"-"+editorIDName;
    $(endEdit).hide();
    $(startEdit).click(function() {
        CKEDITOR.disableAutoInline = true;
        CKEDITOR.%(ckfunction)s( editorIDName, {
            startupFocus: true,
            autoGrow_onStartup: true,
            language: "%(language)s",
            height: %(height)s,
            width: %(width)s,
            codeSnippet_theme: "%(code_theme)s",
            imageUploadUrl: "%(image_upload_url)s",
            filebrowserUploadUrl: "%(file_upload_url)s",
            filebrowserBrowseUrl: "%(file_browser_url)s",
            extraPlugins: "%(extra_plugins)s",
            %(csrf_header)s // CSRF token header for XHR request
            %(custom_config)s
        });
        $(startEdit).hide();
        $(endEdit).show();
        $("#" + editorIDName).attr("contenteditable","true");
    });
    $(endEdit).click(function() {
        if ( CKEDITOR.instances[editorIDName] ) {
            var json_data = {
                csrf_token: "%(csrf_token)s",
                editor_name: editorIDName,
                edit_data: CKEDITOR.instances[editorIDName].getData(),
            };
            $.post("%(post_url)s", json_data);
            CKEDITOR.instances[editorIDName].destroy();
        }
        $(endEdit).hide();
        $(startEdit).show();
        $("#" + editorIDName).attr("contenteditable","false");
    });
});
</script>''' % {
            'name':name, 
            'ckfunction': ckfunction,
            'language':language, 
            'height':height, 
            'width':width,  
            'code_theme':code_theme, 
            'image_upload_url':file_uploader, 
            'file_upload_url':file_uploader, 
            'file_browser_url':file_browser,
            'extra_plugins':','.join(extra_plugins), 
            'csrf_header':csrf_header, 
            'custom_config':custom_config, 
            'csrf_token':current_app.jinja_env.globals['csrf_token'](),
            'post_url':postUrl}

        )


from flask.signals import Namespace


_signals = Namespace()


#: Sent when a user is logged in. In addition to the app (which is the
#: sender), it is passed `user`, which is the user being logged in.
after_loaded = _signals.signal('after-loaded')
before_saved = _signals.signal('before-saved')

from werkzeug.local import LocalProxy

current_editor = LocalProxy(lambda: flask.current_app.extensions.get('inline-editor'))