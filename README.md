<!--
this template is taken from:
 https://gitlab.com/othneildrew/Best-README-Template/blob/master/BLANK_README.md
-->





<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]



<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/flaskless/flask-inline-editor">
    <img src="images/cover.png" alt="Logo">
  </a>

  <h1 align="center">Flask Inline Editor</h1>

  <p align="center">
    Make your HTML content editable
    <br />
    <a href="https://gitlab.com/flaskless/flask-inline-editor"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/flaskless/flask-inline-editor">View Demo</a>
    ·
    <a href="https://gitlab.com/flaskless/flask-inline-editor/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/flaskless/flask-inline-editor/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)


### Features

* Creates inline CKEditor to modify HTML
* Edited HTML is stored in sqlite database
* Includes CSRF protection
* Choose editing permission

<!--Here's a blank template to get started:
**To avoid retyping too much info. Do a search and replace with your text editor for the following:**
`github_username`, `repo`, `twitter_handle`, `email`-->


### Built With

* [Flask-CKEditor](https://flask-ckeditor.readthedocs.io/en/latest/)
* [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)
* [JQuery](https://api.jquery.com/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* python >= 3.8

* pip >= 20.1.1
```sh
pip install --upgrade pip
```

### Installation
 
1. Clone the flask-inline-editor repo
```sh
git clone https://gitlab.com/flaskless/flask-inline-editor.git
```
2. Install the Python packages
```sh
pip install ./flask-inline-editor
```


<!-- USAGE EXAMPLES -->
## Usage

<!--Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.-->


## As a Flask Extension

1. Initialize the extension

```python
import inline_editor
#... create your flask app: app = Flask(__name__)...
inline_editor.init_app(app,storage='edits.db')
```

2. Add the `editablehtml` blocks around the content:

```html
...
<body>
  <div class="content">
    {% editablehtml name="mycontent" %}
      <h1> Hi, this is my content </h1>
    {% endeditablehtml %}
  </div>
</body>
```


_For more examples, please refer to the [Documentation](https://example.com)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/flaskless/flask-inline-editor/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See [`LICENSE`](LICENSE) for more information.

<!-- CONTACT -->
## Contact

FlaskLess Team - [@twitter_handle](https://twitter.com/twitter_handle) - email

Project Link: [https://gitlab.com/flaskless/flask-inline-editor](https://gitlab.com/flaskless/flask-inline-editor)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* []()
* []()
* []()





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=flat-square
[contributors-url]: https://gitlab.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=flat-square
[forks-url]: https://gitlab.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=flat-square
[stars-url]: https://gitlab.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=flat-square
[issues-url]: https://gitlab.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://gitlab.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png

